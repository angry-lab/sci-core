# sci-core


### Быстрый запуск приложения

```bash
git clone https://gitlab.com/angry-lab/sci-core.git
cd sci-core
docker-compose up -d
```

### Сервисы:

- http://localhost:8880/ - лендинг, репозиторий: https://gitlab.com/angry-lab/sci-landing
- http://localhost:8890/ - личный кабинет пользователя, репозиторий: https://gitlab.com/angry-lab/sci-front
- http://localhost:3080/api/ - REST API, текущий репозиторий
- http://localhost:8800/ - расчет рейтинга (REST API), репозиторий: https://gitlab.com/angry-lab/sci-rating-model


### Связанные репозитории:

- https://gitlab.com/angry-lab/baseimage
- https://gitlab.com/angry-lab/sci-ml-models
