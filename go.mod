module gitlab.com/angry-lab/sci-core

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/friendsofgo/errors v0.9.2
	github.com/go-redis/redis/v8 v8.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/labstack/echo/v4 v4.1.17
	github.com/partyzanex/testutils v0.0.11
	github.com/pkg/errors v0.8.1
	github.com/sethgrid/gencurl v0.0.0-20161025011400-a3af93c1aba4
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	github.com/volatiletech/null/v8 v8.1.0
	github.com/volatiletech/randomize v0.0.1
	github.com/volatiletech/sqlboiler/v4 v4.3.1
	github.com/volatiletech/strmangle v0.0.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
