package publication

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/angry-lab/sci-core/api/user"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
)

func (h *Handler) DeletePublication(ctx echo.Context) (err error) {
	publication := &models.Publication{}

	if err := ctx.Bind(publication); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	usr, ok := ctx.Get(user.CtxUserKey).(*models.User)
	if !ok || usr == nil {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	c := ctx.Request().Context()

	tx, err := h.DB.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	model, err := models.FindPublication(c, tx, publication.ID)
	if err != nil {
		return errors.Wrap(err, "cannot load publication")
	}

	if model.UserID != usr.ID {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	_, err = model.Delete(c, tx)
	if err != nil {
		return errors.Wrap(err, "cannot delete publication")
	}

	return ctx.JSON(http.StatusOK, nil)
}
