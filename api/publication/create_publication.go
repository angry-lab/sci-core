package publication

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/angry-lab/sci-core/api/user"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
)

func (h *Handler) CreatePublication(ctx echo.Context) (err error) {
	publication := &ChangeRequest{}

	if err := ctx.Bind(publication); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	usr, ok := ctx.Get(user.CtxUserKey).(*models.User)
	if !ok || usr == nil {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	publication.OrgID = usr.OrgID
	publication.UserID = usr.ID

	c := ctx.Request().Context()

	tx, err := h.DB.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	err = publication.Insert(c, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "cannot create record")
	}

	err = publication.AddUserPublications(c, tx, false, publication.UserPublications()...)
	if err != nil {
		return errors.Wrap(err, "cannot add publication users")
	}

	return ctx.JSON(http.StatusOK, publication)
}

type ChangeRequest struct {
	*models.Publication

	Users []*models.User `json:"users,omitempty"`
}

func (r *ChangeRequest) UserPublications() []*models.UserPublication {
	results := make([]*models.UserPublication, len(r.Users))

	for i, u := range r.Users {
		results[i] = &models.UserPublication{
			PublicationID: r.ID,
			UserID:        u.ID,
		}
	}

	return results
}
