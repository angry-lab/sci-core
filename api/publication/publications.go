package publication

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/angry-lab/sci-core/api/user"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
)

func (h *Handler) Publications(ctx echo.Context) error {
	usr, ok := ctx.Get(user.CtxUserKey).(*models.User)
	if !ok || usr == nil {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	c := ctx.Request().Context()

	err := usr.L.LoadPublications(c, h.DB, true, usr, nil)
	if err != nil {
		return errors.Wrap(err, "cannot load user publications")
	}

	return ctx.JSON(http.StatusOK, usr.R.Publications)
}
