package publication

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
)

func (h *Handler) Categories(ctx echo.Context) error {
	c := ctx.Request().Context()

	categories, err := models.Categories().All(c, h.DB)
	if err == sql.ErrNoRows {
		return ctx.JSON(http.StatusOK, []interface{}{})
	}

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error()).SetInternal(err)
	}

	return ctx.JSON(http.StatusOK, categories)
}
