package user

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/angry-lab/sci-core/db/models"
	"gitlab.com/angry-lab/sci-core/pkg/pipeline"
	"net/http"
)

func (h *Handler) Profile(ctx echo.Context) error {
	user, ok := ctx.Get(CtxUserKey).(*models.User)
	if !ok || user == nil {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	c := ctx.Request().Context()

	err := pipeline.Run(
		func() error {
			return user.L.LoadInterests(c, h.DB, true, user, nil)
		},
		func() error {
			return user.L.LoadOrg(c, h.DB, true, user, nil)
		},
		func() error {
			return user.L.LoadUserAwards(c, h.DB, true, user, nil)
		},
		func() error {
			return user.L.LoadUserProfile(c, h.DB, true, user, nil)
		},
	)
	if err != nil {
		ctx.Logger().Error(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "cannot load user data")
	}

	if user.R == nil {
		return echo.NewHTTPError(http.StatusNotFound, "cannot load user profile")
	}

	user.Password = ""

	return ctx.JSON(http.StatusOK, &Profile{
		User:         user,
		Organization: user.R.Org,
		Profile:      user.R.UserProfile,
		Interests:    user.R.Interests,
		Awards:       user.R.UserAwards,
	})
}

type Profile struct {
	User         *models.User         `json:"user"`
	Organization *models.Organization `json:"organization"`
	Profile      *models.UserProfile  `json:"profile"`
	Interests    []*models.Interest   `json:"interests"`
	Awards       []*models.UserAward  `json:"awards"`
}
