package user

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"github.com/asaskevich/govalidator"
	"github.com/labstack/echo/v4"
	"github.com/partyzanex/testutils"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/angry-lab/sci-core/db/models"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (h *Handler) Register(ctx echo.Context) (err error) {
	req := &RegisterRequest{}

	if err := ctx.Bind(req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	if len(req.Password) <= 6 {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid password")
	}

	if len(req.Login) <= 4 {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid login")
	}

	if !govalidator.IsEmail(req.Email) {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid email")
	}

	c := ctx.Request().Context()

	tx, err := h.DB.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	org, err := models.Organizations(qm.Where("id = ?", req.OrgID)).One(c, tx)
	if err == sql.ErrNoRows || org == nil {
		return echo.NewHTTPError(http.StatusNotFound, "organization not found")
	}

	if err != nil {
		return errors.Wrap(err, "database error")
	}

	p, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrap(err, "invalid password")
	}

	user := models.User{
		OrgID:      org.ID,
		Login:      req.Login,
		Email:      req.Email,
		Password:   string(p),
		FirstName:  req.FirstName,
		SecondName: req.SecondName,
		LastName:   req.LastName,
		CountryID:  org.CountryID,
		CityID:     org.CityID,
		DTCreated:  time.Now(),
		DTUpdated:  time.Now(),
	}

	err = user.Insert(c, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "cannot create user")
	}

	uniq := []string{
		strconv.FormatInt(int64(user.ID), 10),
		strconv.FormatInt(time.Now().Unix(), 10),
		user.Login, testutils.RandomString(32),
	}
	t := sha256.Sum256([]byte(strings.Join(uniq, "_")))

	token := models.Token{
		ID:        hex.EncodeToString(t[:]),
		UserID:    user.ID,
		Type:      models.TokenTypeAuth,
		DTCreated: time.Now(),
		DTExpired: time.Now().Add(60 * 24 * time.Hour),
	}

	err = token.Insert(c, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "cannot create token")
	}

	return ctx.JSON(http.StatusOK, token)
}

type RegisterRequest struct {
	Login      string `json:"login"`
	Email      string `json:"email"`
	Password   string `json:"password"`
	FirstName  string `json:"first_name"`
	SecondName string `json:"second_name"`
	LastName   string `json:"last_name"`
	OrgID      int    `json:"org_id"`
}
