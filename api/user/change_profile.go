package user

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
)

func (h *Handler) ChangeProfile(ctx echo.Context) (err error) {
	req := &ChangeProfile{}

	if err := ctx.Bind(req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	user, ok := ctx.Get(CtxUserKey).(*models.User)
	if !ok || user == nil {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	c := ctx.Request().Context()

	tx, err := h.DB.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	err = user.L.LoadUserProfile(c, tx, true, user, nil)
	if err != nil {
		return errors.Wrap(err, "cannot load user profile")
	}

	if user.R == nil {
		return errors.New("internal error")
	}

	var profile *models.UserProfile

	if user.R.UserProfile == nil {
		profile = &models.UserProfile{
			UserID: user.ID,
		}
	} else {
		profile = user.R.UserProfile
	}

	if profile.UserID != user.ID {
		return echo.NewHTTPError(http.StatusForbidden)
	}

	profile.OrgPost = req.OrgPost
	profile.ScopusEid = req.ScopusEid

	err = profile.Upsert(c, tx, boil.Infer(), boil.Infer())
	if err != nil {
		return errors.Wrap(err, "cannot save user profile")
	}

	return ctx.JSON(http.StatusOK, profile)
}

type ChangeProfile struct {
	*models.UserProfile
}
