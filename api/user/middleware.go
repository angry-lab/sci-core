package user

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
	"time"
)

var (
	XAuthHeader = "X-Auth-Token"
	CtxUserKey  = "user"
	CtxTokenKey = "token"
)

func (h *Handler) AuthMiddleware() echo.MiddlewareFunc {
	return func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			req := ctx.Request()

			t := req.Header.Get(XAuthHeader)
			if t == "" {
				return echo.NewHTTPError(http.StatusBadRequest, "auth token required")
			}

			c := req.Context()

			token, err := models.Tokens(
				qm.Where("id = ? AND type = ?", t, models.TokenTypeAuth),
				qm.Load("User"),
				// qm.Load("User.UserProfile"),
				// qm.Load("User.Org"),
			).One(c, h.DB)
			if err == sql.ErrNoRows || token == nil {
				return echo.NewHTTPError(http.StatusUnauthorized, "token not found")
			}

			if err != nil {
				return errors.Wrap(err, "cannot get token")
			}

			if token.DTExpired.Before(time.Now()) {
				return echo.NewHTTPError(http.StatusGone, "auth expired")
			}

			if token.R == nil || token.R.User == nil {
				return echo.NewHTTPError(http.StatusNotFound, "user not found")
			}

			ctx.Set(CtxTokenKey, token)
			ctx.Set(CtxUserKey, token.R.User)

			return handlerFunc(ctx)
		}
	}
}
