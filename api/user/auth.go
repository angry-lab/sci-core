package user

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"github.com/labstack/echo/v4"
	"github.com/partyzanex/testutils"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/angry-lab/sci-core/db/models"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (h *Handler) Auth(ctx echo.Context) (err error) {
	req := &AuthRequest{}

	if err := ctx.Bind(req); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	c := ctx.Request().Context()

	tx, err := h.DB.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	user, err := models.Users(qm.Where("login = ?", req.Login)).One(c, tx)
	if err == sql.ErrNoRows || user == nil {
		return echo.NewHTTPError(http.StatusNotFound, "user not found")
	}

	if err != nil {
		return errors.Wrap(err, "database error")
	}

	uniq := []string{
		strconv.FormatInt(int64(user.ID), 10),
		strconv.FormatInt(time.Now().Unix(), 10),
		user.Login, testutils.RandomString(32),
	}
	t := sha256.Sum256([]byte(strings.Join(uniq, "_")))

	token := models.Token{
		ID:        hex.EncodeToString(t[:]),
		UserID:    user.ID,
		Type:      models.TokenTypeAuth,
		DTCreated: time.Now(),
		DTExpired: time.Now().Add(60 * 24 * time.Hour),
	}

	err = token.Insert(c, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "cannot create token")
	}

	return ctx.JSON(http.StatusOK, token)
}

type AuthRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
