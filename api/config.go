package api

import (
	"fmt"
	"time"
)

type Config struct {
	Host string
	Port uint16

	DSN          string
	ConnLifetime time.Duration

	ScopusApiKey string

	RedisAddr     string
	RedisPassword string
	RedisDB       uint8
}

func (c Config) Address() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}
