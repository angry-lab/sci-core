package api

import (
	"context"
	"database/sql"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"
	"gitlab.com/angry-lab/sci-core/api/publication"
	"gitlab.com/angry-lab/sci-core/api/user"
	"net/http"
)

func Run(ctx context.Context, config Config) error {
	db, err := sql.Open("mysql", config.DSN)
	if err != nil {
		return errors.Wrap(err, "cannot open connection")
	}

	db.SetConnMaxIdleTime(config.ConnLifetime)

	e := echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())
	e.Use(middleware.CORS())

	huser := user.Handler{
		DB: db,
	}

	hpubs := publication.Handler{
		DB: db,
	}

	e.Any("/api", version)
	e.Any("/api/", version)

	auth := huser.AuthMiddleware()

	users := e.Group("/api/user")
	users.POST("/auth", huser.Auth)
	users.POST("/register", huser.Register)
	users.GET("/profile", huser.Profile, auth)
	users.PUT("/profile", huser.ChangeProfile, auth)

	pubs := e.Group("/api/publications")
	pubs.GET("/categories", hpubs.Categories, auth)
	pubs.GET("/", hpubs.Publications, auth)
	pubs.POST("/", hpubs.CreatePublication, auth)
	pubs.PUT("/", hpubs.UpdatePublication, auth)
	pubs.DELETE("/", hpubs.DeletePublication, auth)

	return e.Start(config.Address())
}

func version(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	}{
		Name:    "Sci API",
		Version: "v0.0.6",
	})
}
