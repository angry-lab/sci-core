CREATE TABLE IF NOT EXISTS country
(
    id   INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    iso3 VARCHAR(3)   NOT NULL,
    iso2 VARCHAR(2)   NOT NULL,
    INDEX `country_name_idx` (`name` ASC) VISIBLE
);

CREATE TABLE IF NOT EXISTS `city`
(
    `id`           INT UNSIGNED   NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(255)   NOT NULL,
    `country_id`   INT UNSIGNED   NOT NULL,
    `country_code` VARCHAR(2)     NOT NULL,
    `latitude`     DECIMAL(10, 8) NOT NULL,
    `longitude`    DECIMAL(10, 8) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_city_country_idx` (`country_id` ASC) VISIBLE,
    CONSTRAINT `fk_city_country`
        FOREIGN KEY (`country_id`)
            REFERENCES `country` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS organization
(
    id          INT UNSIGNED            NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name        CHARACTER VARYING(1000) NOT NULL,
    description TEXT                    NOT NULL,
    country_id  INT UNSIGNED            NOT NULL,
    city_id     INT UNSIGNED            NOT NULL,
    profile     JSON DEFAULT NULL,
    CONSTRAINT `fk_org_country`
        FOREIGN KEY (`country_id`)
            REFERENCES `country` (`id`)
            ON DELETE NO ACTION
            ON UPDATE CASCADE,
    CONSTRAINT `fk_org_city`
        FOREIGN KEY (`city_id`)
            REFERENCES `city` (`id`)
            ON DELETE NO ACTION
            ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS `user`
(
    `id`             BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `org_id`         INT UNSIGNED        NOT NULL,
    `login`          VARCHAR(128)        NOT NULL,
    `email`          VARCHAR(128)        NOT NULL,
    `password`       VARCHAR(64)         NOT NULL,
    `avatar`         VARCHAR(255)        NULL,
    `first_name`     VARCHAR(96)         NOT NULL,
    `last_name`      VARCHAR(96)         NOT NULL,
    `birthday`       DATE                NOT NULL,
    `country_id`     INT UNSIGNED        NOT NULL,
    `city_id`        INT UNSIGNED        NOT NULL,
    `description`    MEDIUMTEXT          NOT NULL,
    `dt_created`     TIMESTAMP           NOT NULL DEFAULT now(),
    `dt_updated`     TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `dt_last_logged` TIMESTAMP                    DEFAULT NULL,
    `settings`       JSON                NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
    UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
    CONSTRAINT `fk_user_org`
        FOREIGN KEY (`org_id`)
            REFERENCES `organization` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS user_profile
(
    user_id  BIGINT UNSIGNED PRIMARY KEY NOT NULL,
    rating   INT DEFAULT NULL,
    org_post ENUM (
        'asp', 'ass', 'vns', 'gns', 'doctorant',
        'doc', 'mns', 'ns', 'prep', 'stprep',
        'stajor', 'sns', 'stud', 'adm')  NOT NULL,
    pubs     INT DEFAULT NULL,
    awards   INT DEFAULT NULL,
    CONSTRAINT `fk_user_profile_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS publication
(
    id          BIGINT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    org_id      INT UNSIGNED                NOT NULL,
    user_id     BIGINT UNSIGNED             NOT NULL,
    name        VARCHAR(1000)               NOT NULL,
    data        LONGBLOB                    NOT NULL,
    description TEXT                        NOT NULL,
    citations   INT UNSIGNED DEFAULT NULL,
    CONSTRAINT `fk_publication_org`
        FOREIGN KEY (`org_id`)
            REFERENCES `organization` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,
    CONSTRAINT fk_publication_user_id
        FOREIGN KEY (user_id)
            REFERENCES user (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS category
(
    id   INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS publication_category
(
    publication_id BIGINT UNSIGNED NOT NULL,
    category_id    INT UNSIGNED    NOT NULL,
    PRIMARY KEY (publication_id, category_id),
    CONSTRAINT `fk_p2p_id`
        FOREIGN KEY (`publication_id`)
            REFERENCES `publication` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `fk_p2c_id`
        FOREIGN KEY (`category_id`)
            REFERENCES `category` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS user_publication
(
    id             BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    publication_id BIGINT UNSIGNED NOT NULL,
    user_id        BIGINT UNSIGNED NOT NULL,
    CONSTRAINT fk_user_publication_user_id
        FOREIGN KEY (user_id)
            REFERENCES user (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT fk_user_publication_id
        FOREIGN KEY (publication_id)
            REFERENCES publication (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS interest
(
    id   INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS user_interest
(
    user_id     BIGINT UNSIGNED NOT NULL,
    interest_id INT UNSIGNED    NOT NULL,
    PRIMARY KEY (user_id, interest_id),
    CONSTRAINT `fk_ui_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `fk_ui_interest_id`
        FOREIGN KEY (`interest_id`)
            REFERENCES `interest` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS user_award
(
    id         BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name       VARCHAR(1000)   NOT NULL,
    user_id    BIGINT UNSIGNED NOT NULL,
    meaning    INT             NOT NULL,
    sort_order INT             NOT NULL DEFAULT 0,
    CONSTRAINT `fk_award_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS `token`
(
    `id`         VARCHAR(64)                        NOT NULL,
    `user_id`    BIGINT(20) UNSIGNED                NOT NULL,
    `type`       ENUM ('auth', 'reset', 'register') NOT NULL,
    `dt_created` DATETIME                           NOT NULL DEFAULT now(),
    `dt_expired` DATETIME                           NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_token_user_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_token_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;


ALTER TABLE user
    ADD COLUMN second_name VARCHAR(128) NOT NULL AFTER first_name;

ALTER TABLE user
    MODIFY birthday DATE DEFAULT NULL;
