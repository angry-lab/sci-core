DROP TABLE IF EXISTS user_profile;
DROP TABLE IF EXISTS user_interest;
DROP TABLE IF EXISTS user_publication;
DROP TABLE IF EXISTS user_award;
DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS publication_category;
DROP TABLE IF EXISTS publication;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS interest;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS organization;
DROP TABLE IF EXISTS city;
DROP TABLE IF EXISTS country;
