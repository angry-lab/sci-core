DROP TABLE IF EXISTS scopus_author;
DROP TABLE IF EXISTS publons_author;
DROP TABLE IF EXISTS publication_to_publons;
DROP TABLE IF EXISTS publons_publication;

ALTER TABLE user_profile
    DROP COLUMN scopus_eid,
    DROP COLUMN orcid;
