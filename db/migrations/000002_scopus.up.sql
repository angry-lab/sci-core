CREATE TABLE IF NOT EXISTS scopus_author
(
    id             BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    orcid          VARCHAR(64)     NOT NULL,
    eid            VARCHAR(64)     NOT NULL,
    name           VARCHAR(255)    NOT NULL,
    document_count INT             NOT NULL,
    user_id        BIGINT UNSIGNED DEFAULT NULL,
    affiliation_id INT             DEFAULT NULL,
    CONSTRAINT eid_uk UNIQUE (eid),
    CONSTRAINT `fk_scopus_author_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

ALTER TABLE user_profile
    ADD COLUMN scopus_eid VARCHAR(64) DEFAULT NULL,
    ADD COLUMN orcid      VARCHAR(64) DEFAULT NULL;

CREATE TABLE IF NOT EXISTS publons_author
(
    publon_id                     BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    orcid                         VARCHAR(64)              DEFAULT NULL,
    rid                           VARCHAR(64)              DEFAULT NULL,
    truid                         VARCHAR(64)              DEFAULT NULL,
    user_id                       BIGINT UNSIGNED          DEFAULT NULL,
    name                          TEXT            NOT NULL,
    names                         JSON                     DEFAULT NULL,
    bio                           TEXT                     DEFAULT NULL,
    photo                         VARCHAR(1000)            DEFAULT NULL,
    affiliations                  JSON                     DEFAULT NULL,
    reviews_pre                   INT             NOT NULL,
    reviews_post                  INT             NOT NULL,
    publications                  INT             NOT NULL,
    editorial_boards              INT             NOT NULL,
    handling_editor_records       INT             NOT NULL,
    dt_created                    TIMESTAMP       NOT NULL DEFAULT now(),
    datetime_records_last_updated TIMESTAMP                DEFAULT NULL,
    CONSTRAINT publons_id_uk UNIQUE (publon_id),
    CONSTRAINT publons_orcid_uk UNIQUE (orcid),
    CONSTRAINT `fk_publons_author_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `user` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS publons_publication
(
    publon_id         BIGINT UNSIGNED NOT NULL PRIMARY KEY,
    publons_author_id BIGINT UNSIGNED NOT NULL,
    ut                VARCHAR(64) DEFAULT NULL,
    doi               VARCHAR(64) DEFAULT NULL,
    pmid              VARCHAR(64) DEFAULT NULL,
    ar_xiv            VARCHAR(64) DEFAULT NULL,
    title             TEXT            NOT NULL,
    date_published    DATE        DEFAULT NULL,
    journal           JSON        DEFAULT NULL,
    publisher         JSON        DEFAULT NULL,
    CONSTRAINT publons_ut_uk UNIQUE (ut),
    CONSTRAINT publons_doi_uk UNIQUE (doi),
    CONSTRAINT publons_pmid_uk UNIQUE (pmid),
    CONSTRAINT publons_ar_xiv_uk UNIQUE (ar_xiv)
);

# CREATE TABLE IF NOT EXISTS publication_to_publons
# (
#     publication_id BIGINT UNSIGNED NOT NULL,
#     publon_id      BIGINT UNSIGNED NOT NULL,
#     PRIMARY KEY (publication_id, publon_id),
#     CONSTRAINT `fk_p2p_publication_id`
#         FOREIGN KEY (`publication_id`)
#             REFERENCES `publication` (`id`)
#             ON DELETE CASCADE
#             ON UPDATE CASCADE,
#     CONSTRAINT `fk_p2p_publon_id`
#         FOREIGN KEY (`publon_id`)
#             REFERENCES `publons_publication` (`publon_id`)
#             ON DELETE CASCADE
#             ON UPDATE CASCADE
# );
