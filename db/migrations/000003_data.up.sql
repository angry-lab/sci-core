insert into country set id = 1, name = 'Российская Федерация', iso2 = 'RU', iso3 = 'RUS';
insert into city set id = 1, name = 'Москва', country_id = 1, country_code = 'RU', latitude = '55.755814', longitude = '37.617635';
insert into city set id = 2, name = 'Ростов-на-дону', country_id = 1, country_code = 'RU', latitude = '47.222078', longitude = '39.720349';
insert into organization set id = 1, name = 'Rostov State University of Economics', country_id = 1, city_id = 2, description = 'Федеральное государственное бюджетное образовательное учреждение высшего образования "Ростовский государственный экономический университет (РИНХ)"';
