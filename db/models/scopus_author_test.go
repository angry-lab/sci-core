// Code generated by SQLBoiler 4.3.1 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/randomize"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testScopusAuthors(t *testing.T) {
	t.Parallel()

	query := ScopusAuthors()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testScopusAuthorsDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testScopusAuthorsQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := ScopusAuthors().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testScopusAuthorsSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := ScopusAuthorSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testScopusAuthorsExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := ScopusAuthorExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if ScopusAuthor exists: %s", err)
	}
	if !e {
		t.Errorf("Expected ScopusAuthorExists to return true, but got false.")
	}
}

func testScopusAuthorsFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	scopusAuthorFound, err := FindScopusAuthor(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if scopusAuthorFound == nil {
		t.Error("want a record, got nil")
	}
}

func testScopusAuthorsBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = ScopusAuthors().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testScopusAuthorsOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := ScopusAuthors().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testScopusAuthorsAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	scopusAuthorOne := &ScopusAuthor{}
	scopusAuthorTwo := &ScopusAuthor{}
	if err = randomize.Struct(seed, scopusAuthorOne, scopusAuthorDBTypes, false, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}
	if err = randomize.Struct(seed, scopusAuthorTwo, scopusAuthorDBTypes, false, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = scopusAuthorOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = scopusAuthorTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := ScopusAuthors().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testScopusAuthorsCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	scopusAuthorOne := &ScopusAuthor{}
	scopusAuthorTwo := &ScopusAuthor{}
	if err = randomize.Struct(seed, scopusAuthorOne, scopusAuthorDBTypes, false, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}
	if err = randomize.Struct(seed, scopusAuthorTwo, scopusAuthorDBTypes, false, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = scopusAuthorOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = scopusAuthorTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func scopusAuthorBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func scopusAuthorAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *ScopusAuthor) error {
	*o = ScopusAuthor{}
	return nil
}

func testScopusAuthorsHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &ScopusAuthor{}
	o := &ScopusAuthor{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, false); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor object: %s", err)
	}

	AddScopusAuthorHook(boil.BeforeInsertHook, scopusAuthorBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	scopusAuthorBeforeInsertHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.AfterInsertHook, scopusAuthorAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	scopusAuthorAfterInsertHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.AfterSelectHook, scopusAuthorAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	scopusAuthorAfterSelectHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.BeforeUpdateHook, scopusAuthorBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	scopusAuthorBeforeUpdateHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.AfterUpdateHook, scopusAuthorAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	scopusAuthorAfterUpdateHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.BeforeDeleteHook, scopusAuthorBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	scopusAuthorBeforeDeleteHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.AfterDeleteHook, scopusAuthorAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	scopusAuthorAfterDeleteHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.BeforeUpsertHook, scopusAuthorBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	scopusAuthorBeforeUpsertHooks = []ScopusAuthorHook{}

	AddScopusAuthorHook(boil.AfterUpsertHook, scopusAuthorAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	scopusAuthorAfterUpsertHooks = []ScopusAuthorHook{}
}

func testScopusAuthorsInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testScopusAuthorsInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(scopusAuthorColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testScopusAuthorToOneUserUsingUser(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local ScopusAuthor
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	queries.Assign(&local.UserID, foreign.ID)
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.User().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if !queries.Equal(check.ID, foreign.ID) {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := ScopusAuthorSlice{&local}
	if err = local.L.LoadUser(ctx, tx, false, (*[]*ScopusAuthor)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.User == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.User = nil
	if err = local.L.LoadUser(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.User == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testScopusAuthorToOneSetOpUserUsingUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a ScopusAuthor
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, scopusAuthorDBTypes, false, strmangle.SetComplement(scopusAuthorPrimaryKeyColumns, scopusAuthorColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetUser(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.User != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.ScopusAuthors[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if !queries.Equal(a.UserID, x.ID) {
			t.Error("foreign key was wrong value", a.UserID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.UserID))
		reflect.Indirect(reflect.ValueOf(&a.UserID)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if !queries.Equal(a.UserID, x.ID) {
			t.Error("foreign key was wrong value", a.UserID, x.ID)
		}
	}
}

func testScopusAuthorToOneRemoveOpUserUsingUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a ScopusAuthor
	var b User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, scopusAuthorDBTypes, false, strmangle.SetComplement(scopusAuthorPrimaryKeyColumns, scopusAuthorColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err = a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = a.SetUser(ctx, tx, true, &b); err != nil {
		t.Fatal(err)
	}

	if err = a.RemoveUser(ctx, tx, &b); err != nil {
		t.Error("failed to remove relationship")
	}

	count, err := a.User().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 0 {
		t.Error("want no relationships remaining")
	}

	if a.R.User != nil {
		t.Error("R struct entry should be nil")
	}

	if !queries.IsValuerNil(a.UserID) {
		t.Error("foreign key value should be nil")
	}

	if len(b.R.ScopusAuthors) != 0 {
		t.Error("failed to remove a from b's relationships")
	}
}

func testScopusAuthorsReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testScopusAuthorsReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := ScopusAuthorSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testScopusAuthorsSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := ScopusAuthors().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	scopusAuthorDBTypes = map[string]string{`ID`: `bigint`, `Orcid`: `varchar`, `Eid`: `varchar`, `Name`: `varchar`, `DocumentCount`: `int`, `UserID`: `bigint`, `AffiliationID`: `int`}
	_                   = bytes.MinRead
)

func testScopusAuthorsUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(scopusAuthorPrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(scopusAuthorAllColumns) == len(scopusAuthorPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testScopusAuthorsSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(scopusAuthorAllColumns) == len(scopusAuthorPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &ScopusAuthor{}
	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, scopusAuthorDBTypes, true, scopusAuthorPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(scopusAuthorAllColumns, scopusAuthorPrimaryKeyColumns) {
		fields = scopusAuthorAllColumns
	} else {
		fields = strmangle.SetComplement(
			scopusAuthorAllColumns,
			scopusAuthorPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := ScopusAuthorSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testScopusAuthorsUpsert(t *testing.T) {
	t.Parallel()

	if len(scopusAuthorAllColumns) == len(scopusAuthorPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}
	if len(mySQLScopusAuthorUniqueColumns) == 0 {
		t.Skip("Skipping table with no unique columns to conflict on")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := ScopusAuthor{}
	if err = randomize.Struct(seed, &o, scopusAuthorDBTypes, false); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert ScopusAuthor: %s", err)
	}

	count, err := ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, scopusAuthorDBTypes, false, scopusAuthorPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize ScopusAuthor struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert ScopusAuthor: %s", err)
	}

	count, err = ScopusAuthors().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
