package queue

import (
	"context"
	"io"
)

type Message struct {
	Body []byte `json:"body"`
}

type Queue interface {
	io.Closer

	Flush() error
	Send(ctx context.Context, msg interface{}) error
	Receive(ctx context.Context) (<-chan Message, error)
}
