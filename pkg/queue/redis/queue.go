package redis

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/angry-lab/sci-core/pkg/queue"
)

var (
	ErrRequiredClient  = errors.New("required redis client option")
	ErrRequiredChannel = errors.New("required channel option")
)

type Option func(qh *queueHandler)

func WithClient(client *redis.Client) Option {
	return func(qh *queueHandler) {
		qh.client = client
	}
}

func WithContext(ctx context.Context) Option {
	return func(qh *queueHandler) {
		qh.ctx = ctx
	}
}

func WithChannel(name string) Option {
	return func(qh *queueHandler) {
		qh.channel = name
	}
}

type queueHandler struct {
	client  *redis.Client
	ctx     context.Context
	cancel  context.CancelFunc
	channel string
	ps      *redis.PubSub
}

func New(options ...Option) (queue.Queue, error) {
	qh := &queueHandler{}

	for _, option := range options {
		option(qh)
	}

	if qh.client == nil {
		return nil, ErrRequiredClient
	}

	if qh.channel == "" {
		return nil, ErrRequiredChannel
	}

	if qh.ctx == nil {
		qh.ctx, qh.cancel = context.WithCancel(context.TODO())
	}

	qh.ps = qh.client.Subscribe(qh.ctx, qh.channel)

	_, err := qh.ps.Receive(qh.ctx)
	if err != nil {
		return nil, errors.Wrap(err, "cannot wait for confirmation")
	}

	return qh, nil
}

func (qh *queueHandler) Close() error {
	if qh.cancel != nil {
		qh.cancel()
	}

	if qh.ps != nil {
		return qh.ps.Close()
	}

	return nil
}

func (qh *queueHandler) Flush() error {
	return nil
}

func (qh *queueHandler) Send(ctx context.Context, msg interface{}) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "cannot marshalling message")
	}

	err = qh.client.Publish(qh.ctx, qh.channel, string(b)).Err()
	if err != nil {
		return errors.Wrap(err, "cannot publish message")
	}

	return nil
}

func (qh *queueHandler) Receive(ctx context.Context) (<-chan queue.Message, error) {
	msgs := make(chan queue.Message)
	ch := qh.ps.Channel()

	go func() {
		for raw := range ch {
			msg := queue.Message{
				Body: []byte(raw.Payload),
			}

			msgs <- msg
		}

		close(msgs)
	}()

	return msgs, nil
}
