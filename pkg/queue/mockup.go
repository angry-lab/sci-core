package queue

import "context"

type Mock struct {
	SendFunc    func(msg interface{}) error
	ReceiveFunc func(ctx context.Context) (<-chan Message, error)
}

func (mock *Mock) Send(msg interface{}) error {
	return mock.SendFunc(msg)
}

func (mock *Mock) Receive(ctx context.Context) (<-chan Message, error) {
	return mock.ReceiveFunc(ctx)
}

func (Mock) Flush() error {
	return nil
}

func (Mock) Close() error {
	return nil
}
