package pipeline

type PipeFunc func() error

func Run(pipelines ...PipeFunc) error {
	for _, pipeline := range pipelines {
		if err := pipeline(); err != nil {
			return err
		}
	}

	return nil
}
