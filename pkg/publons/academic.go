package publons

type Response struct {
	Count int     `json:"count"`
	Next  *string `json:"next"`
	Prev  *string `json:"prev"`
}

type AcademicList struct {
	Response
	Results []*Academic `json:"results"`
}

type Academic struct {
	IDs                        IDs          `json:"ids"`
	PublishingName             string       `json:"publishing_name"`
	PublishingNames            []string     `json:"publishing_names"`
	Bio                        string       `json:"bio"`
	Photo                      string       `json:"photo"`
	Affiliations               Affiliations `json:"affiliations"`
	Reviews                    Reviews      `json:"reviews"`
	Publications               Count        `json:"publications"`
	EditorialBoards            Count        `json:"editorial_boards"`
	HandlingEditorRecords      Count        `json:"handling_editor_records"`
	DatetimeRecordsLastUpdated *Time        `json:"datetime_records_last_updated"`
}

type IDs struct {
	ID    int64  `json:"id"`
	ORCID string `json:"orcid"`
	RID   string `json:"rid"`
	TruID string `json:"truid"`
	URL   string `json:"url"`
	API   string `json:"api"`
	UT    string `json:"ut"`
	DOI   string `json:"doi"`
	PMID  string `json:"pmid"`
	ArXiv string `json:"arXiv"`
	ISSN  string `json:"issn"`
	EISSN string `json:"eissn"`
}

type Affiliations struct {
	Institutions []Affiliation `json:"institutions"`
	Other        []Affiliation `json:"other"`
}

type Affiliation struct {
	IDs  IDs    `json:"ids"`
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Reviews struct {
	Pre  Count `json:"pre"`
	Post Count `json:"post"`
}

type Count struct {
	Count int    `json:"count"`
	URL   string `json:"url"`
}
