package publons_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/angry-lab/sci-core/pkg/publons"
	"os"
	"testing"
)

func TestClient_GetAcademics(t *testing.T) {
	apiKey := os.Getenv("TEST_API_KEY")
	if apiKey == "" {
		t.Skip()
	}

	client := publons.Client{
		ApiKey: apiKey,
	}

	academics, err := client.GetAcademics("Rostov State University of Economics")
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(academics) > 0)
}

func TestClient_GetAcademicByID(t *testing.T) {
	apiKey := os.Getenv("TEST_API_KEY")
	if apiKey == "" {
		t.Skip()
	}

	client := publons.Client{
		ApiKey: apiKey,
	}

	academic, err := client.GetAcademicByID("1606823")
	assert.Equal(t, nil, err)
	assert.Equal(t, true, academic != nil)
}

func TestClient_GetPublications(t *testing.T) {
	apiKey := os.Getenv("TEST_API_KEY")
	if apiKey == "" {
		t.Skip()
	}

	client := publons.Client{
		ApiKey: apiKey,
	}

	academics, err := client.GetAcademics("Rostov State University of Economics")
	assert.Equal(t, nil, err)

	articles, err := client.GetPublications(academics[1].IDs.ORCID)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(articles.Results) > 0)
}
