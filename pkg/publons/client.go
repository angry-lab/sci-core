package publons

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

const (
	BaseURL = "https://publons.com/api/v2"
)

type Client struct {
	ApiKey string
}

func (c *Client) GetAcademics(institution string) ([]*Academic, error) {
	url := fmt.Sprintf("%s/academic/", BaseURL)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	values := req.URL.Query()
	values.Add("institution", institution)
	req.URL.RawQuery = values.Encode()

	c.setHeaders(req)

	results := make([]*Academic, 0)

Request:
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	list := &AcademicList{}

	// err = json.Unmarshal(b, &list)
	err = json.NewDecoder(resp.Body).Decode(&list)
	if err != nil {
		return nil, err
	}

	if list.Next != nil {
		req, err = http.NewRequest(http.MethodGet, *list.Next, nil)
		if err != nil {
			return nil, err
		}

		results = append(results, list.Results...)

		c.setHeaders(req)
		<-time.After(5 * time.Second)
		goto Request
	}

	return results, nil
}

func (c Client) setHeaders(r *http.Request) {
	r.Header.Set("Authorization", "Token "+c.ApiKey)
	r.Header.Set("Content-Type", "application/json")
}

func (c *Client) GetAcademicByID(id string) (*Academic, error) {
	url := fmt.Sprintf("%s/academic/%s", BaseURL, id)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	c.setHeaders(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	result := &Academic{}

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *Client) GetPublications(id string) ([]*Article, error) {
	url := fmt.Sprintf("%s/academic/publication/", BaseURL)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	values := req.URL.Query()
	values.Add("academic", id)
	req.URL.RawQuery = values.Encode()

	c.setHeaders(req)

	results := make([]*Article, 0)

Request:
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	// b, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println(string(b))
	list := &ArticleList{}

	// err = json.Unmarshal(b, &result)
	err = json.NewDecoder(resp.Body).Decode(&list)
	if err != nil {
		return nil, err
	}

	if list.Next != nil {
		req, err = http.NewRequest(http.MethodGet, *list.Next, nil)
		if err != nil {
			return nil, err
		}

		results = append(results, list.Results...)

		c.setHeaders(req)
		<-time.After(5 * time.Second)
		goto Request
	}

	return results, nil
}
