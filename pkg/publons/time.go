package publons

import (
	"strings"
	"time"
)

const (
	TimeLayout = "2006-01-02T15:04:05.999999"
)

var (
	replacer = strings.NewReplacer("\"", "")
)

type Time time.Time

func (t *Time) UnmarshalJSON(b []byte) error {
	d, err := time.ParseInLocation(TimeLayout, replacer.Replace(string(b)), time.UTC)
	if err != nil {
		return err
	}

	*t = Time(d)

	return nil
}
