package publons

type ArticleList struct {
	Response
	Results []*Article `json:"results"`
}

type Article struct {
	IDs         IDs         `json:"ids"`
	Publication Publication `json:"publication"`
	Author      IDs         `json:"author"`
	Journal     *Journal    `json:"journal"`
	Publisher   *Publisher  `json:"publisher"`
}

type Publication struct {
	IDs           IDs    `json:"ids"`
	Title         string `json:"title"`
	DatePublished string `json:"date_published"`
}

type Journal struct {
	Name string `json:"name"`
	IDs  IDs    `json:"ids"`
}

type Publisher struct {
	Name string `json:"name"`
	IDs  IDs    `json:"ids"`
}
