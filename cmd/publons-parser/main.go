package main

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	version = "v0.0.1"
)

func main() {
	app := &cli.App{
		Name:    "publons-parser",
		Usage:   "publons parser",
		Version: version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "dsn",
				EnvVars:  []string{"SCI_MYSQL_DSN", "DATABASE_URL"},
				Required: true,
			},
			&cli.DurationFlag{
				Name:  "conn-lifetime",
				Value: time.Second,
			},
			&cli.StringFlag{
				Name:     "api-key",
				Required: true,
				EnvVars:  []string{"PUBLONS_API_KEY"},
			},
		},
		Before: func(ctx *cli.Context) error {
			db, err := sql.Open("mysql", ctx.String("dsn"))
			if err != nil {
				return errors.Wrap(err, "cannot open connection")
			}

			db.SetConnMaxIdleTime(ctx.Duration("conn-lifetime"))
			ctx.Context = context.WithValue(ctx.Context, "db", db)

			return nil
		},
		Action: ParsePublons,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
