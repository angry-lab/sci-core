package main

import (
	"context"
	"database/sql"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/angry-lab/sci-core/db/models"
	"gitlab.com/angry-lab/sci-core/pkg/publons"
)

func ParsePublons(ctx *cli.Context) error {
	log.Println("start parsing")

	db, ok := ctx.Context.Value("db").(*sql.DB)
	if !ok || db == nil {
		return errors.New("required db")
	}

	client := publons.Client{
		ApiKey: ctx.String("api-key"),
	}

	orgs, err := models.Organizations().All(ctx.Context, db)
	if err != nil {
		return errors.Wrap(err, "cannot find org")
	}

	log.Printf("found %d orgs", len(orgs))

	savedAuthors := make(chan []*models.PublonsAuthor)
	errs := make([]error, 0)

	wg := sync.WaitGroup{}
	wg.Add(1)

	c, cancel := context.WithCancel(ctx.Context)
	defer cancel()

	go func() {
		defer wg.Done()

		for {
			select {
			case <-ctx.Done():
				return
			case authors, ok := <-savedAuthors:
				if !ok && authors == nil {
					return
				}

				log.Printf("saving %d authors", len(authors))

				for _, author := range authors {
					id := author.Orcid.String
					if !author.Orcid.Valid {
						id = strconv.FormatInt(int64(author.PublonID), 10)
					}

					articles, err := client.GetPublications(id)
					if err != nil {
						errs = append(errs, err)
						continue
					}

					log.Printf("recieved %d articles", len(articles))

					err = ensurePublonsPublications(c, db, author, articles)
					if err != nil {
						errs = append(errs, err)
					}
				}
			}
		}
	}()

	for _, org := range orgs {
		log.Printf("load authors for org %d", org.ID)

		academics, err := client.GetAcademics(org.Name)
		if err != nil {
			return errors.Wrap(err, "cannot get academics")
		}

		log.Printf("received %d authors for org %d", len(academics), org.ID)

		authors, err := ensurePublonsAuthors(c, db, org, academics)
		if err != nil {
			return errors.Wrap(err, "cannot save academics")
		}

		savedAuthors <- authors
	}

	close(savedAuthors)
	wg.Wait()

	if n := len(errs); n > 0 {
		e := make([]string, n)

		for i, err := range errs {
			e[i] = err.Error()
		}

		return errors.New(strings.Join(e, "; "))
	}

	return nil
}

func ensurePublonsPublications(c context.Context, db *sql.DB, author *models.PublonsAuthor, articles []*publons.Article) error {
	log.Println("save publications")

	tx, err := db.Begin()
	if err != nil {
		return errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	for _, article := range articles {
		dt, _ := time.ParseInLocation("2006-01-02", article.Publication.DatePublished, time.UTC)

		model := models.PublonsPublication{
			PublonID:        uint64(article.IDs.ID),
			PublonsAuthorID: author.PublonID,
			Ut:              null.NewString(article.IDs.UT, article.IDs.UT != ""),
			Doi:             null.NewString(article.IDs.DOI, article.IDs.DOI != ""),
			Pmid:            null.NewString(article.IDs.PMID, article.IDs.PMID != ""),
			ArXiv:           null.NewString(article.IDs.ArXiv, article.IDs.ArXiv != ""),
			Title:           article.Publication.Title,
			DatePublished:   null.NewTime(dt, !dt.IsZero()),
		}

		if err := model.Journal.Marshal(article.Journal); err != nil {
			return err
		}

		if err := model.Publisher.Marshal(article.Publisher); err != nil {
			return err
		}

		err := model.Upsert(c, tx, boil.Blacklist("publons_id"), boil.Infer())
		if err != nil {
			return err
		}
	}

	return nil
}

func ensurePublonsAuthors(c context.Context, db *sql.DB,
	org *models.Organization, authors []*publons.Academic) (results []*models.PublonsAuthor, err error) {
	log.Printf("save authors for org %d", org.ID)
	now := time.Now().UTC()

	tx, err := db.Begin()
	if err != nil {
		return nil, errors.Wrap(err, "cannot begin transaction")
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}

		err = tx.Commit()
	}()

	results = make([]*models.PublonsAuthor, len(authors))

	for i, academic := range authors {
		var (
			userID = null.Uint64{}
			names  = null.JSON{}
			dt     time.Time
		)

		if academic.IDs.ORCID != "" {
			profile, err := models.UserProfiles(qm.Where("orcid = ?", academic.IDs.ORCID)).One(c, tx)
			if err != nil && err != sql.ErrNoRows {
				return nil, errors.Wrap(err, "cannot get user profile by orcid")
			}

			if profile != nil {
				userID.SetValid(profile.UserID)
			}
		}

		if err := names.Marshal(academic.PublishingNames); err != nil {
			return nil, errors.Wrap(err, "cannot encode names")
		}

		if academic.DatetimeRecordsLastUpdated != nil {
			dt = time.Time(*academic.DatetimeRecordsLastUpdated)
		}

		model := &models.PublonsAuthor{
			PublonID:                   uint64(academic.IDs.ID),
			Orcid:                      null.NewString(academic.IDs.ORCID, academic.IDs.ORCID != ""),
			Rid:                        null.NewString(academic.IDs.RID, academic.IDs.RID != ""),
			Truid:                      null.NewString(academic.IDs.TruID, academic.IDs.TruID != ""),
			UserID:                     userID,
			Name:                       academic.PublishingName,
			Names:                      names,
			Bio:                        null.NewString(academic.Bio, academic.Bio != ""),
			Photo:                      null.NewString(academic.Photo, academic.Photo != ""),
			ReviewsPre:                 academic.Reviews.Pre.Count,
			ReviewsPost:                academic.Reviews.Post.Count,
			Publications:               academic.Publications.Count,
			EditorialBoards:            academic.EditorialBoards.Count,
			HandlingEditorRecords:      academic.HandlingEditorRecords.Count,
			DTCreated:                  now,
			DatetimeRecordsLastUpdated: null.NewTime(dt, !dt.IsZero()),
		}

		if err := model.Affiliations.Marshal(academic.Affiliations); err != nil {
			return nil, errors.Wrap(err, "cannot encode affiliations")
		}

		err := model.Upsert(c, tx, boil.Blacklist("publon_id"), boil.Infer())
		if err != nil {
			return nil, errors.Wrap(err, "cannot encode affiliations")
		}

		results[i] = model
	}

	return results, nil
}
