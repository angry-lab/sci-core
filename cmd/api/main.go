package main

import (
	"log"
	"os"
	"time"

	// using mysql
	_ "github.com/go-sql-driver/mysql"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/angry-lab/sci-core/api"
)

const (
	version = "v0.0.6"
)

func main() {
	app := &cli.App{
		Name:    "api",
		Usage:   "sci-core REST API server",
		Version: version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "host",
				EnvVars:    []string{"HTTP_HOST", "SCI_API_HOST"},
				Value:      "0.0.0.0",
				HasBeenSet: true,
				Required:   true,
			},
			&cli.UintFlag{
				Name:       "port",
				EnvVars:    []string{"HTTP_PORT", "SCI_API_PORT"},
				Value:      3080,
				HasBeenSet: true,
				Required:   true,
			},
			&cli.StringFlag{
				Name:     "dsn",
				EnvVars:  []string{"SCI_MYSQL_DSN", "DATABASE_URL"},
				Required: true,
			},
			&cli.DurationFlag{
				Name:  "conn-lifetime",
				Value: time.Second,
			},
		},
		Action: func(ctx *cli.Context) error {
			err := api.Run(ctx.Context, api.Config{
				Host:         ctx.String("host"),
				Port:         uint16(ctx.Uint("port")),
				DSN:          ctx.String("dsn"),
				ConnLifetime: ctx.Duration("conn-lifetime"),
			})
			if err != nil {
				return errors.Wrap(err, "run api")
			}

			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
